

const cachekey = 'NUMBER';

if (typeof Storage !== 'undefined') {
    
    if (localStorage.getItem(cachekey) === 'undefined') {
        
        localStorage.setItem(cachekey, 0);
    }

    const button = document.querySelector('#button');
    const clear = document.querySelector('#clear');
    const count = document.querySelector('#count');

    button.addEventListener('click', function (event) {
        
        let number = localStorage.getItem(cachekey);
        number++;

        localStorage.setItem(cachekey, number);
        count.innerText = localStorage.getItem(cachekey);
    });

    clear.addEventListener('click', function (event) {
        localStorage.removeItem(cachekey);
    });

} else {
    alert('browser anda tidak mendukung');
}