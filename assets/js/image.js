let catImage = document.querySelector('#catImage');

let caption = document.querySelector("#caption");
caption.innerHTML = '<em>Tiga anak kucing</em>'

let newElement = document.createElement('p');
newElement.innerHTML = 'anda menekan gambar ini sebanyak <span id="count">0</span> kali';
// createElement hanya saja membuat element baru, untuk memunculkannya kedalam body perlu di tambahkan appenchild() seperti kode dibawah
document.body.appendChild(newElement);

catImage.addEventListener('click', function(event) {
    document.querySelector('#count').innerText++;
});
