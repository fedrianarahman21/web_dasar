
const cachekey = 'NUMBER';

if (typeof (Storage) !== 'undefined') {
    // pengecekkan apakah data sessionStorage dengan key NUMBER tersedia atau belum

    if (sessionStorage.getItem(cachekey) === 'undefined') {

        sessionStorage.setItem(cachekey, 0);
    }

    const button = document.querySelector('#button');
    const count = document.querySelector('#count');

    button.addEventListener('click', function (event) {
        
        let number = sessionStorage.getItem(cachekey);
        number++;

        sessionStorage.setItem(cachekey, number);
        count.innerText = sessionStorage.getItem(cachekey);
    });
}else{
    alert('browser tidak mendukung web storage');
}

